const getIp = 'https://api.ipify.org/?format=json';
const personInfo='http://ip-api.com/json/';
const root = document.querySelector('#root')
const btn = document.createElement('button');
btn.innerText = 'Знайти по IP'
root.append(btn)

const sendRequest = async (url, method = "GET", config) => {
  return await fetch(url, {
      method,
      ...config
  }).then(response => {
      if(response.ok) {
        return response.json()
      }
  })
}

const renderPersonInfo = async (url) => {
	const responsePerson = await sendRequest(url)
  const personIP = await responsePerson.ip;
  const getInfo = await sendRequest(`${personInfo}${personIP}`)
  // console.log(getInfo);
  const {country, city, regionName, timezone, region} = getInfo;

  root.insertAdjacentHTML('beforeend', `
  <ul>
  <li>Country:${country}</li>
      <li>City: ${city}</li>
      <li>Region name: ${regionName}</li>
      <li>Timezone: ${timezone}</li>
      <li>Region: ${region}</li>
      </ul>
  `)
}

btn.addEventListener('click', ()=>{
  renderPersonInfo(getIp)
})


